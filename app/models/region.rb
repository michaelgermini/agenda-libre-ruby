# This is mostly to group events around a region
class Region < ActiveRecord::Base
  has_many :orgas
  has_many :events

  default_scope { order :name }

  def to_s
    name
  end
end
