# All the mail and tweet related callbacks to event's lifecycle
class EventCallbacks
  def self.after_create(event)
    EventMailer.create(event).deliver_now!
    ModerationMailer.create(event).deliver_now!
  end

  def self.after_update(event)
    if event.moderated_changed?
      tweet(event)

      # Send an acceptation mail to its author
      EventMailer.accept(event).deliver_now

      # Send an acceptation mail to moderators
      ModerationMailer.accept(event).deliver_now
    else
      # Send an update mail to moderators
      ModerationMailer.update(event).deliver_now
    end
  end

  def self.after_destroy(event)
    EventMailer.destroy(event).deliver_now
    ModerationMailer.destroy(event).deliver_now
  end

  # Tweet this event, if configured using apache/system variables!
  def self.tweet(event)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
      config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
      config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
      config.access_token_secret = ENV['TWITTER_ACCESS_SECRET']
    end
    client.update event.to_tweet if client.consumer_key
  end
end
