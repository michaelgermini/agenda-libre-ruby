# This is the central ADL class, where are managed all events
class Event < ActiveRecord::Base
  extend SimpleCalendar
  strip_attributes
  has_paper_trail ignore: [:last_updated, :secret, :submitter, :decision_time,
                           :lock_version, :latitude, :longitude]

  belongs_to :region
  has_many :notes, dependent: :destroy

  validates :title, presence: true
  validate :end_after_start
  validates :description, presence: true
  validates :city, presence: true
  validates :region, presence: true
  validates :url, presence: true, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :contact, email: true
  validates :submitter, email: true
  validates :tags, presence: true, format: /\A[\p{Alnum}\s-]*\z/

  geocoded_by :full_address
  # after_validation :geocode, if: -> (obj) { obj.address_changed? }
  after_validation :geocode

  # Mechanism to store some reason which can be used when sending notifications
  attr_accessor :reason

  after_create EventCallbacks
  after_update EventCallbacks
  after_destroy EventCallbacks

  scope :moderated, ->(*) { where moderated: true }
  scope :unmoderated, ->(*) { where moderated: false }
  scope :last_year, -> { where '? <= end_time', 1.year.ago }
  scope :past, -> { where 'start_time <= ?', Time.zone.now }
  scope :future, -> { where '? <= end_time', Time.zone.now }
  scope :daylimit, ->(d) { where 'end_time <= ?', d.to_i.days.from_now }
  scope :year, (lambda do |year|
    where '? <= end_time and start_time <= ?',
          Date.new(year.to_i, 1, 1).beginning_of_week,
          Date.new(year.to_i, 12, 31).end_of_week.end_of_day
  end)
  scope :month, (lambda do |start_date|
    where '? <= end_time and start_time <= ?',
          start_date.to_date.beginning_of_month.beginning_of_week,
          start_date.to_date.end_of_month.end_of_week.end_of_day
  end)
  scope :period, (lambda do |year, week|
    start_date = DateTime.commercial(year.to_i, week.to_i)
    where '? <= end_time and start_time <= ?',
          start_date, start_date.end_of_week.end_of_day
  end)
  scope :region, ->(region) { where region: region unless region == 'all' }
  scope :locality, ->(locality) { where locality: locality }
  scope :tag, ->(tag) { where 'tags like ?', "%#{tag}%" }
  scope :geo, -> { where 'latitude is not null and longitude is not null' }

  before_validation do
    # Tags are always downcased
    self.tags = tags.mb_chars.downcase if tags
  end

  before_validation on: :create do
    self.submission_time = Time.zone.now
    self.decision_time = Time.zone.now

    # Populate submitter using contact info if absent
    self.submitter = contact if submitter.blank?
  end

  before_validation on: :update do
    self.latitude = nil if address_changed?
    self.longitude = nil if address_changed?
  end

  before_create do
    self.secret = SecureRandom.urlsafe_base64(32)[0...32]
    self.moderator_mail_id = SecureRandom.urlsafe_base64(32)[0...32]
    self.submitter_mail_id = SecureRandom.urlsafe_base64(32)[0...32]
  end

  before_update do
    self.decision_time = Time.zone.now if moderated? && moderated_changed?
  end

  def as_json(_options = {})
    { type: 'Feature', properties: {
      id: id, name: title, start_time: start_time, end_time: end_time,
      place_name: place_name, address: address, city: city, locality: locality,
      tags: tags,
      popupContent: "<a href=\"/events/#{id}\">#{self}</a>"
    }, geometry: { type: 'Point', coordinates: [longitude, latitude] } }
  end

  def full_address
    # Region seems to disturb openstreetmap :(
    # [address, city, region.try(:name)].compact.join ', '
    [address, city].compact.join ', '
  end

  def hashtags
    tags.split.map { |tag| "##{tag.tr('-', '_').camelize :lower}" }
  end

  def to_s
    "#{start_time.to_date} #{city}: #{title} #{hashtags.join(' ')}"
  end

  def to_tweet
    url = Rails.application.routes.url_helpers.event_url(
      self,
      host: ActionMailer::Base.default_url_options[:host]
    )

    tweet = "#{self} #{url}"
    if tweet.size >= 140
      tweet = "#{tweet[0, tweet.rindex(/\s/, 140 - url.size)]} #{url}"
    end
    tweet
  end

  private

  def end_after_start
    errors.add :end_time, :before_start if
      !end_time.blank? && !start_time.blank? && end_time <= start_time
  end
end
