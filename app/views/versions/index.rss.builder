require 'differ/format/patch'
Differ.format = Differ::Format::Patch

xml.instruct!

xml.rdf :RDF,
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'xmlns' => 'http://purl.org/rss/1.0/',
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
        'xmlns:sy' => 'http://purl.org/rss/1.0/modules/syndication/',
        'xmlns:admin' => 'http://webns.net/mvcb/',
        'xmlns:cc' => 'http://web.resource.org/cc/',
        'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:georss' => 'http://www.georss.org/georss' do
  xml.channel 'rdf:about' => root_url do
    title = t 'layouts.application.title'
    if params[:region].present? && params[:region] != 'all'
      region = Region.find(params[:region]).name
      title += " [#{region}]"
    end
    xml.title title
    xml.description t 'layouts.application.subtitle'
    xml.link root_url
    xml.dc :language, 'fr'
    xml.dc :creator, root_url

    xml.items do
      xml.rdf :Seq do
        @versions.each do |version|
          xml.rdf :li, 'rdf:resource' =>
            polymorphic_url(version.item_type.tableize.singularize,
                            id: version.item_id)
        end
      end
    end
  end

  @versions.each do |version|
    if version.event == 'create'
      object = version.item_type.constantize.find_by_id version.item_id
      next unless object.try(:moderated?)
    end
    object = version.reify if object.nil?
    url = polymorphic_url(version.item_type.tableize.singularize,
                          id: version.item_id)

    xml.item 'rdf:about' => url do
      xml.title "#{version.event} - #{object}"
      xml.link url
      domain = root_url.gsub(/www/, '').gsub(/http.?:../, '').gsub(/:.*/, '')
      xml.dc :identifier, "version_#{version.id}@#{domain}"
      xml.dc :date, version.created_at.iso8601

      if object.try(:description)
        if version.event == 'create'
          xml.description strip_tags object.description
          xml.content(:encoded) { xml.cdata! object.description }
        elsif version.event == 'update'
          changes = ''
          version.changeset.each do |key, val|
            changes += version.item_type.constantize.human_attribute_name(key)
            changes += ': '
            if key == 'description' && val[0].present? && val[1].present?
              changes += Differ.diff(val[1], val[0]).to_s
            else
              changes += "#{val[0]} -> #{val[1]}"
            end
            changes += '
            '
          end
          xml.description changes
          xml.content(:encoded) { xml.cdata! changes }
        end
      end
    end
  end
end
