$(document).on 'turbolinks:load', ->
  # Manage event tags edition
  $('#event_tags').each ->
    elt = $(this)
    $.ajax
      url: '/tags.json'
    .done (data) ->
      tags = jQuery.map data, (n) -> n[0]

      elt.select2 tags: tags, separator: [' '], tokenSeparators: [' ']

  # Manage the tags label so it points the proper select2 input
  $('label[for=event_tags]').attr 'for', 's2id_autogen1'

  $('#event_start_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_end_time').val($('#event_start_time').val())
  $('#event_end_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_start_time').val($('#event_end_time').val())
