$(document).on 'turbolinks:load', ->
  # Manage orga tags edition
  $('#orga_tags').each ->
    elt = $(this)
    $.ajax
      url: '/tags/orgas.json'
    .done (data) ->
      tags = jQuery.map data, (n) -> n[0]

      elt.select2 tags: tags, separator: [' '], tokenSeparators: [' ']

  # Manage the tags label so it points the proper select2 input
  $('label[for=orga_tags]').attr 'for', 's2id_autogen1'
