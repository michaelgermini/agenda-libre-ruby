# Event management life cycle
class ModerationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_moderation, :set_mailer_host, only:
    [:show, :edit, :preview, :update, :validate, :accept, :refuse, :destroy]
  rescue_from ActiveRecord::StaleObjectError, with: :locked

  def index
    @events = Event.unmoderated
    @orgas = Orga.unmoderated
  end

  def preview
    @moderation.attributes = moderation_params
    @moderation.valid?
    render action: :edit
  end

  # PATCH/PUT /moderations/1
  # PATCH/PUT /moderations/1.json
  def update
    respond_to do |format|
      if @moderation.update_attributes moderation_params
        format.html { redirect_to :moderations, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        # 422 means :unprocessable_entity
        format.json { render json: @moderation.errors, status: 422 }
      end
    end
  end

  # PATCH/PUT /accept/1
  # PATCH/PUT /accept/1.json
  def accept
    @moderation.update moderated: true
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @moderation.reason = params[:event][:reason] ||
                         t("moderations.refuse.reason_#{params[:reason]}_long")
    @moderation.destroy!
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_moderation
    @event = Event.find params[:id]
    @moderation = @event
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def moderation_params
    params.require(:event)
          .permit :lock_version, :title, :start_time, :end_time, :description,
                  :place_name, :address, :city, :region_id, :locality, :url,
                  :contact, :submitter, :tags
  end

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  def locked
    redirect_to edit_moderation_url(@moderation), alert: t('staleObjectError')
  end
end
