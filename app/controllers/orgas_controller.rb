# Groups life cycle
class OrgasController < ApplicationController
  has_scope :region

  before_action :set_orga, except: [:index, :new, :create]
  before_action :set_mailer_host
  before_action :authenticate_user!, except: [:index, :new, :create, :show],
                                     unless: :check_secret

  def index
    @search = apply_scopes(Orga).moderated.includes(:kind,
                                                    :region).search params[:q]
    @search.sorts = 'updated_at desc' if @search.sorts.empty?
    @orgas = @search.result.page params[:page]
  end

  # GET /orgas/new
  def new
    @orga = Orga.new
  end

  # POST /orgas
  # POST /orgas.json
  def create
    @orga = Orga.new orga_params
    respond_to do |format|
      if @orga.save
        format.html { redirect_to :root, notice: t('.ok') }
        # 201 means :created
        format.json { render action: 'show', status: 201, location: @orga }
      else
        format.html { render action: 'new' }
        # 422 means :unprocessable_entity
        format.json { render json: @orga.errors, status: 422 }
      end
    end
  end

  def show
    @search = Orga.search params[:q]

    @events_future = Event.moderated.future.tag @orga.name_as_tag
    @events_past = Event.moderated.past.tag @orga.name_as_tag
  end

  # PATCH/PUT /orgas/1
  # PATCH/PUT /orgas/1.json
  def update
    respond_to do |format|
      if @orga.update orga_params
        format.html { redirect_to @orga, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        # 422 means :unprocessable_entity
        format.json { render json: @orga.errors, status: 422 }
      end
    end
  end

  # PATCH/PUT /accept/1
  # PATCH/PUT /accept/1.json
  def accept
    @orga.update moderated: true
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  # DELETE /orgas/1
  # DELETE /orgas/1.json
  def destroy
    @orga.destroy
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_orga
    @orga = Orga.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through
  def orga_params
    params.require(:orga)
          .permit :lock_version, :kind_id, :name, :description, :place_name,
                  :address, :city, :department, :region_id, :url, :diaspora,
                  :feed, :contact, :submitter, :tags, :active
  end

  # Check that you can only edit an existing event if you know its secret
  def check_secret
    !%w(validate refuse).include?(action_name) &&
      (!@orga.secret || @orga.secret == params[:secret])
  end
end
