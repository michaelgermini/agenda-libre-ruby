# Geocoding
#
# Access to OSM controls
class MapsController < ApplicationController
  has_scope :region, :locality, :tag, :daylimit
  has_scope :future, type: :boolean, default: true
  has_scope :period, type: :hash, using: [:year, :week]
  has_scope :near, type: :hash, using: [:location, :distance]

  def index
    respond_to do |format|
      format.html
      format.json { render json: apply_scopes(Event.moderated.geo) }
    end
  end

  def show
    @kind = Kind.find_by_name params[:id]
    respond_to do |format|
      format.html
      format.json do
        render json: apply_scopes(Orga.moderated.active.geo).where(kind: @kind)
      end
    end
  end
end
