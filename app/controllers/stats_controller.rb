# Generate statistics, around events, by date or place
class StatsController < ApplicationController
  before_action :set_temporal, :set_local, only: [:index]

  private

  def set_temporal
    @years = Event.group(year_grouping).count
    @months = Event.group(year_grouping, month_grouping).count
  end

  def set_local
    @regions = Event.joins(:region).group(:region_id, year_grouping).count
    @city_events = Event.group(:city).having('count(city) > 3')
                        .order('count(city) desc').count
  end

  def year_grouping
    if %w(Mysql2 MySQL PostgreSQL).include? Event.connection.adapter_name
      'extract(year from start_time)'
    elsif Event.connection.adapter_name == 'SQLite'
      'strftime("%Y", start_time)'
    end
  end

  def month_grouping
    if %w(Mysql2 MySQL PostgreSQL).include? Event.connection.adapter_name
      'extract(month from start_time)'
    elsif Event.connection.adapter_name == 'SQLite'
      'strftime("%m", start_time)'
    end
  end
end
