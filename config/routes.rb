Rails.application.routes.draw do
  get 'application/infos'
  get 'application/contact'
  get 'application/rules'
  get 'stats', to: 'stats#index'
  get 'versions', to: 'versions#index'

  resources :users
  resources :events do
    get :cancel, on: :member
    post :preview, on: :collection, to: 'events#preview_create'
    patch :preview, on: :member
  end
  resources :moderations do
    resources :notes, only: [:new, :create]
    patch :preview, on: :member
    get :validate, :refuse, on: :member
    put :accept, on: :member
  end
  resources :digests, only: [:index, :show]
  resources :regions, only: [:index]
  get 'tags/orgas', to: 'tags#orgas'
  resources :tags, only: [:index, :show]
  resources :maps, only: [:index, :show]
  resources :orgas do
    get :cancel, :validate, :refuse, on: :member
    put :accept, on: :member
  end

  # Manage former php pages
  get 'showevent.php', to: redirect { |_, req| "events/#{req.params[:id]}" }
  get 'listevents.php', to: redirect { |_, req| "events?#{req.query_string}" }
  get 'editevent.php', to: redirect(lambda do |_, req|
    "events/#{req.params[:id]}/edit?secret=#{req.params[:secret]}"
  end)
  # A mechanism to list "all" events
  get '(:format)listevents.php',
      to: redirect { |_, r| "events.#{r.format.to_sym}?#{r.query_string}" }
  # Respond to rss and ical calls
  get 'event.php',
      to: redirect { |_, r| "events?#{r.query_string}" }
  get '(:format)event.php',
      to: redirect { |_, r| "events.#{r.format.to_sym}?#{r.query_string}" }
  # Respond to rss and ical calls
  get '(:format).php',
      to: redirect { |_, r| "events.#{r.format.to_sym}?#{r.query_string}" }

  devise_for :users,
             path: '', path_names: { sign_in: 'login', sign_out: 'logout' }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: 'events#index'

  # The priority is based upon order of creation:
  # first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions
  # automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
