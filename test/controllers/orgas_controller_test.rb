require 'test_helper'

# Free Software groups life cycle
class OrgasControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @orga = orgas :one
  end

  test 'should get index' do
    get :index
    assert_response :success
  end

  test 'should get new' do
    sign_in users(:one)
    get :new
    assert_response :success
  end

  test 'should create orga' do
    sign_in users(:one)
    assert_difference 'Orga.count' do
      post :create, orga: {
        kind_id: @orga.kind_id,
        name: @orga.name,
        city: @orga.city,
        region_id: @orga.region.id,
        url: @orga.url,
        feed: @orga.feed,
        contact: @orga.contact,
        submitter: @orga.contact
      }

      assert_empty assigns(:orga).errors.messages
    end

    assert_redirected_to :root
  end

  test 'should not create orga' do
    sign_in users(:one)
    assert_no_difference 'Orga.count' do
      post :create, orga: { url: @orga.url }

      assert_not_empty assigns(:orga).errors.messages
    end
  end

  test 'should get show' do
    get :show, id: @orga
    assert_response :success
  end

  test 'should update orga' do
    # Necessary to have the proper paper_trail version
    @orga.update_attributes name: 'My Title'

    patch :update, id: @orga, secret: @orga.secret, orga: { name: @orga.name }

    assert_empty assigns(:orga).errors.messages
    assert_redirected_to assigns(:orga)
  end

  test 'should not update orga' do
    sign_in users(:one)
    patch :update, id: @orga, orga: { name: nil }

    assert_not_empty assigns(:orga).errors.messages
  end

  test 'should not update orga without proper secret' do
    patch :update, id: @orga, orga: {
      name: 'hello world'
    }

    assert_redirected_to :new_user_session
  end
end
