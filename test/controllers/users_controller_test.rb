require 'test_helper'

# Test moderator management controller
class UsersControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @user = users(:one)

    sign_in users(:one)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create user' do
    assert_difference('User.count') do
      post :create, user: {
        email: 'original@example.com',
        firstname: @user.firstname,
        lastname: @user.lastname,
        login: @user.login,
        password: 'abcdefghijklmnopqrstuvwxyz'
      }
    end

    assert_redirected_to user_path(assigns(:user))
  end

  test 'should not create user' do
    assert_no_difference('User.count') do
      post :create, user: {
        login: nil
      }
    end
  end

  test 'should show user' do
    get :show, id: @user
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @user
    assert_response :success
  end

  test 'should update user' do
    patch :update, id: @user, user: {
      email: @user.email,
      firstname: @user.firstname,
      lastname: @user.lastname,
      login: @user.login
    }
    assert_redirected_to user_path(assigns(:user))
  end

  test 'should not update user' do
    patch :update, id: @user, user: {
      login: nil
    }

    assert_not_empty assigns(:user).errors
  end

  test 'should destroy user' do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user
    end

    assert_redirected_to users_path
  end
end
