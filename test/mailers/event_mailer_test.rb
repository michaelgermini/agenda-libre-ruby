require 'test_helper'

# Test mails related to event life cycle
class EventMailerTest < ActionMailer::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
  end

  test 'create' do
    mail = EventMailer.create Event.last
    assert_match(/Votre événement: .* est en attente de modération/,
                 mail.subject)
    assert_equal [Event.last.contact], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
    assert_match(/Bonjour.*/, mail.body.encoded)
  end

  test 'accept' do
    mail = EventMailer.accept Event.last
    assert_match(/Événement .* modéré/, mail.subject)
    assert_equal [Event.last.contact], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end

  test 'destroy' do
    event = Event.last
    event.reason = 'hello world'
    mail = EventMailer.destroy Event.last
    assert_match(/Événement .* refusé/, mail.subject)
    assert_equal [Event.last.contact], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end
end
